module Admin::PhotosHelper
  def photo_is_selected
    (@photo_album && @photo_album.photo_ids.include?(photo.id))
  end
end
