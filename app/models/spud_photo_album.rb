class SpudPhotoAlbum < ActiveRecord::Base
  has_many :spud_photo_albums_photos, dependent: :destroy
  has_many :photos,
           -> { order('spud_photo_albums_photos.sort_order asc') },
           through: :spud_photo_albums_photos,
           source: :spud_photo

  has_many :spud_photo_galleries_albums
  has_many :galleries,
           through: :spud_photo_galleries_albums,
           source: :spud_photo_gallery

  validates :title, :url_name, presence: true
  validates :title, :url_name, uniqueness: true
  before_validation :set_url_name
  after_save :update_photo_order

  scope :ordered, -> { order('created_at desc') }

  def top_photo_url(style)
    return photos.first.photo.url(style) unless photos.empty?
  end

  def photos_available
    if photo_ids.any?
      SpudPhoto.where('id not in (?)', photo_ids)
    else
      SpudPhoto.all
    end
  end

  private

  def set_url_name
    self.url_name = title.parameterize
  end

  def update_photo_order
    # order = 0
    # self.photos.each do |p|
    #   p.update_attribute(:order, order)
    #   order += 1
    # end
  end
end
