class SpudPhotoGallery < ActiveRecord::Base
  has_many :spud_photo_galleries_albums, dependent: :destroy
  has_many :albums,
           through: :spud_photo_galleries_albums,
           source: :spud_photo_album

  validates :title, :url_name, presence: true
  validates :title, :url_name, uniqueness: true
  before_validation :set_url_name

  scope :ordered, -> { order('created_at desc') }

  def top_photo_url(style)
    return albums.first.top_photo_url(style) unless albums.empty?
  end

  def albums_available
    if album_ids.any?
      SpudPhotoAlbum.where('id not in (?)', album_ids)
    else
      SpudPhotoAlbum.all
    end
  end

  private

  def set_url_name
    self.url_name = title.parameterize if title
  end
end
