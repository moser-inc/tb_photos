class PhotoAlbumsController < ApplicationController
  respond_to :html, :json, :xml
  layout Spud::Photos.base_layout

  before_action :gallery if Spud::Photos.galleries_enabled

  def index
    @photo_albums = if @photo_gallery
                      @photo_gallery.albums.ordered
                    else
                      SpudPhotoAlbum.ordered
                    end
    respond_with @photo_albums
  end

  def show
    @photo_album = SpudPhotoAlbum.find_by(url_name: params[:id])
    if @photo_album.blank?
      raise Spud::NotFoundError, item: 'photo album'
    else
      respond_with @photo_album
    end
  end

  private

  def gallery
    @photo_gallery = SpudPhotoGallery.find_by(url_name: params[:photo_gallery_id])
    raise Spud::NotFoundError, item: 'photo gallery' if @photo_gallery.blank?
  end
end
