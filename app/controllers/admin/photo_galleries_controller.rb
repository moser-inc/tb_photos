class Admin::PhotoGalleriesController < Admin::ApplicationController
  before_action :gallery, only: [:show, :edit, :update, :destroy]
  before_action :albums, only: [:new, :create, :edit, :update]
  add_breadcrumb 'Photo Galleries', :admin_photo_galleries_path
  layout 'admin/spud_photos'
  belongs_to_spud_app :photo_galleries

  def index
    @photo_galleries = SpudPhotoGallery.all
    respond_with @photo_galleries
  end

  def show
    respond_with @photo_gallery
  end

  def new
    @photo_gallery = SpudPhotoGallery.new
    respond_with @photo_gallery
  end

  def create
    @photo_gallery = SpudPhotoGallery.new(photo_gallery_params)
    flash[:notice] = 'Gallery created successfully' if @photo_gallery.save
    respond_with @photo_gallery, location: admin_photo_galleries_path
  end

  def edit
    respond_with @photo_gallery
  end

  def update
    @photo_gallery.update_attributes(photo_gallery_params)
    flash[:notice] = 'Gallery updated successfully' if @photo_gallery.save
    respond_with @photo_gallery, location: admin_photo_galleries_path
  end

  def destroy
    flash.now[:notice] = 'Gallery deleted successfully' if @photo_gallery.destroy
    respond_with @photo_gallery, location: admin_photo_galleries_path
  end

  private

  def gallery
    @photo_gallery = SpudPhotoGallery.find(params[:id])
  end

  def albums
    @photo_albums = SpudPhotoAlbum.all
  end

  def photo_gallery_params
    params.require(:spud_photo_gallery).permit(:title, :url_name, album_ids: [])
  end
end
