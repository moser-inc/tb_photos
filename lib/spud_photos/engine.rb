require 'tb_core'
require 'paperclip'

module Spud
  module Photos
    class Engine < Rails::Engine
      engine_name :tb_photos
      initializer :assets_photos do |_config|
        TbCore.append_admin_javascripts('admin/photos/application')
        TbCore.append_admin_stylesheets('admin/photos/application')
      end
      initializer :admin do
        Rails.application.config.assets.precompile += %w( admin/photos/photo_albums_thumb.png )
        TbCore.config.admin_applications += [{
          name: 'Photo Albums',
          thumbnail: 'admin/photos/photo_albums_thumb.png',
          url: '/admin/photo_albums',
          retina: true,
          order: 82
        }]
        if Spud::Photos.config.galleries_enabled
          Spud::Core.config.admin_applications += [{
            name: 'Photo Galleries',
            thumbnail: 'admin/photos/photo_albums_thumb.png',
            url: '/admin/photo_galleries',
            retina: true,
            order: 81
          }]
        end
      end
    end
  end
end
